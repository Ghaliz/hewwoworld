package com.ghala.hewwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HewwoApplication {

	public static void main(String[] args) {
		SpringApplication.run(HewwoApplication.class, args);
	}

}
